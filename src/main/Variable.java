package main;

public class Variable implements Comparable<Variable>{
		public boolean signe;
		public final int id;
		public static int cpt = 0;
		public Variable() {
			this.id = cpt;
			cpt++;
			signe = true;
		}
		@Override
		public int compareTo(Variable other) {
			if (this.id > other.id)
				return 1;
			if (this.id < other.id)
				return -1;
			return 0;
		}
		public String toString() {
			return "X"+id;
		}
	}

