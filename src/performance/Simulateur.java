package performance;

import utils.Helper;
import main.Formule;

public class Simulateur{
	public double duree = 0.0;
	public int nbSolutions = 0;
	private boolean extension;
	private Formule formule;
	
	public Simulateur(Formule f, boolean extension) {
		//super();
		this.extension = extension;
		this.formule = f;
	}
	
	public void tester() {
		//Helper.afficherListeVar(formule.variables);
		//System.out.println("Formule : "+formule.afficher());
		if(extension)
			formule.etendre();
		formule.enleverDoublons();
		double debut = (System.currentTimeMillis()/1000.0);
		formule.trouverSolutions();
		double fin = (System.currentTimeMillis()/1000.0);
		System.out.println(fin - debut);
		System.out.println(formule.solutions.nombreSolutions()+ " solutions\nfin.");
	}
}
