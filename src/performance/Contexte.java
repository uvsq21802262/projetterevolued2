package performance;

import java.util.LinkedList;
import java.util.List;

import stockage.ABR;
import stockage.HMap;
import stockage.ISolution;
import stockage.Liste;
import stockage.ListeTriee;
import stockage.Trie;
import utils.DNFGenerator;
import main.Formule;
import main.Terme;
import main.Variable;

public class Contexte {
	int tailleMinTerme = 1;
	int nbMaxTermes = 3;
	double probaNot = 0.3;
	double probaVar = 0.5;
	int nbVar;
	
	private List<ISolution> stockage;
	private List<Terme.grayStrategy> strategy;
	private boolean[] tab = {false, true};
	public Contexte(int nbVar) {
		this.nbVar = nbVar;
		init();
	}
	private void init(){
		stockage = new LinkedList<ISolution>();
		stockage.add(new ABR());
		stockage.add(new HMap(nbVar));
		stockage.add(new Liste());
		stockage.add(new ListeTriee());
		stockage.add(new Trie(nbVar));
		strategy = new LinkedList<Terme.grayStrategy>();
		//strategy.add(Terme.grayStrategy.ITER1);
		strategy.add(Terme.grayStrategy.ITER2);
		strategy.add(Terme.grayStrategy.REC);

//		for(ISolution sol : stockage) {
//			sol.setBavard(true);
//		}
	}
	
	public void simulation(int iter){
		for(int i = 0; i<iter ; i++) {
			for(ISolution sol : stockage) {
				for(Terme.grayStrategy strat : strategy) {
					for(boolean alea : tab){//aleatoire ou non
						for(boolean extension: tab){//non etendu ou pas
							System.out.print("------------STOCKAGE "+sol+", ");
							System.out.print("STRATEGIE "+strat+", ");
							System.out.print("alea : "+alea+", ");
							System.out.println("ext : "+extension+"-----------");
							Formule formule = DNFGenerator.generateDNF2(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, alea).addSolutionStrategy(sol).addGrayStrategy(strat);
							Simulateur testeur = new Simulateur(formule, extension);
//							Formule formule = DNFGenerator.generateDNF2(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, true).addSolutionStrategy(sol).addGrayStrategy(strat);
//							Simulateur testeur = new Simulateur(formule, false);
							testeur.tester();
							Variable.cpt = 0;//on remet à 0 le cpt de variable
							sol.reset();
						}
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		Contexte t = new Contexte(5);
		t.simulation(1);
	}
}
