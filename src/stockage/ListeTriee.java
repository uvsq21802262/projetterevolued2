package stockage;

import main.Solution;

	public class ListeTriee extends Liste{
		
	public ListeTriee(){
		super();
		nom +=" triée";
	}
	@Override
	public void ajouterSolution(Solution solution) {
		super.ajouterSolutionAff(solution);
		//System.out.print("ajout ");solution.afficher();
		if(super.solutions.isEmpty()) {
			ajouter(0, solution);
			nbSolutions++;
			return;
		}
		int compare = solution.compareTo(super.solutions.get(0));
		if(compare == -1) {
			ajouter(0, solution);
			nbSolutions++;
			return;
		}
		for (int i=0; i<super.solutions.size() ; i++){
			compare = solution.compareTo(solutions.get(i));
			if (compare == 0){
				return ;//est deja dans la liste
			}
			if (compare == -1) {
				ajouter(i, solution);
				nbSolutions++;
				return;
			}
		}
		super.solutions.add(super.solutions.size(), solution);
		nbSolutions++;
	}
	@Override
    public String toString(){
		return "Liste Triée";
    }
}
