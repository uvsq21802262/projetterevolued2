package utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import main.Formule;
import main.Terme;
import main.Variable;

public class DNFGenerator {
	//genere dans l'ordre
	//probanot probabilité qu'une variable soit negative
	//probava probabilité d'apparition d'une variable
	//

	
	public static Formule generateDNF2(int nbVar, int nbTermes, int tailleMinTerme, 
			double probaNot, double probaVar, boolean aleatoire) {

		HashMap<Variable, Object> variables = new HashMap<Variable, Object>();
		Formule formule = new Formule(nbVar);
		VariableFactory vf = new VariableFactory(nbVar, aleatoire);
		for (int i = 0 ; i < nbTermes; i++) {
			Terme terme = new Terme();
				int j = 0;
				while(j < nbVar){
					Variable var = vf.getVariable();
					//formule.ajouterVariable(var);
					variables.put(var, null);
					if (Math.random() < probaVar)
						terme.ajouterVariable(var, Math.random() < 1 - probaNot);
					j++;
				}
				if(terme.taille() > tailleMinTerme)
					formule.ajouterTerme(terme);

				vf.reset();
			}
		formule.variables = new ArrayList<Variable>(variables.keySet());
		Collections.sort(formule.variables);
		formule.triee = !aleatoire;


		vf.reset();
		return formule;
		
	}
	
//	public static Formule formuleTest() {
//		VariableFactory vf = new VariableFactory(3, true);
//		
//		Formule f = new Formule(3);
//		f.triee = false;
//		
//		f.variables = new ArrayList<Variable>();
//		f.variables.add(vf.getVariable(0));
//		f.variables.add(vf.getVariable(1));
//		f.variables.add(vf.getVariable(2));
//		Terme t = new Terme();
//		t.ajouterVariable(vf.getVariable(1), true);
//		t.ajouterVariable(vf.getVariable(0), true);
//		t.ajouterVariable(vf.getVariable(2), true);
//		f.ajouterTerme(t);
//		
//		t = new Terme();
//		t.ajouterVariable(vf.getVariable(1), false);
//		t.ajouterVariable(vf.getVariable(0), true);
//		f.ajouterTerme(t);
//		
//		t = new Terme();
//		t.ajouterVariable(vf.getVariable(2), true);
//		t.ajouterVariable(vf.getVariable(0), false);
//		f.ajouterTerme(t);
//		
//		t = new Terme();
//		t.ajouterVariable(vf.getVariable(2), false);
//		f.ajouterTerme(t);
//		
//		return f;
//	}
//	
}

